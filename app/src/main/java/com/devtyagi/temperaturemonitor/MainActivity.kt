package com.devtyagi.temperaturemonitor

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.devtyagi.temperaturemonitor.databinding.ActivityMainBinding
import com.github.angads25.toggle.interfaces.OnToggledListener
import com.github.angads25.toggle.model.ToggleableView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import to.freebots.temperaturegauge.TemperatureRangeConfig

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var database: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        database = Firebase.database.reference

        binding.gauge.config = arrayListOf(
            TemperatureRangeConfig(
                20F, // max temperature of the range
                Color.GREEN, // color of range
                "Cool!", // info text (optional)
            ),
            TemperatureRangeConfig(
                40F, Color.YELLOW, "Warm",
            ),
            TemperatureRangeConfig(
                90F, Color.RED, "Danger|",
            ),
        )

        binding.gauge.minTemperature = 0F
        binding.gauge.maxTemperature = 90F
        binding.gauge.temperatureTextColor = Color.WHITE

        binding.toggle.colorOff = Color.RED
        binding.toggle.colorOn = Color.GREEN
        binding.toggle.textSize = 22

        binding.toggle.setOnToggledListener { toggleableView, isOn ->
            if(isOn) {
                turnOn()
            } else {
                turnOff()
            }
        }


        database.child("temp").addValueEventListener(object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                "Current Temperature: ${snapshot.value.toString()}°C".also { binding.tempText.text = it }
                binding.gauge.temperature = snapshot.value.toString().toFloat()
            }

            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(this@MainActivity, error.message, Toast.LENGTH_SHORT).show()
            }

        })

        database.child("running").addValueEventListener(object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                binding.toggle.isOn = snapshot.value.toString().toInt() == 1
            }

            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(this@MainActivity, error.message, Toast.LENGTH_SHORT).show()
            }

        })
    }

    fun turnOff() {
        database.child("running").setValue(0)
    }

    fun turnOn() {
        database.child("running").setValue(1)
    }

}